@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Yeni İş</h6>
            </div>
            <div class="card-body">
                <form action="{{url('/works')}}" method="POST" >
                    @csrf
               <div class="form-group row">
                   <label  class="col-sm-4 col-form-label text-center">Atanan</label>
                   <div class="col-sm-6">
                       <select class="form-control" name="assigned" required>
                           <option value="">Seçiniz</option>
                           @foreach($users as $user)
                               <option value="{{$user->id}}">{{$user->name}}</option>
                           @endforeach
                       </select>

                   </div>
               </div>
               <div class="form-group row">
                   <label  class="col-sm-4 col-form-label text-center">Başlık</label>
                   <div class="col-sm-6">
                       <input type="text" class="form-control" name="title"  placeholder="Başlık " required>
                   </div>
               </div>
               <div class="form-group row">
                   <label class="col-sm-4 col-form-label text-center">Açıklama</label>
                   <div class="col-sm-6">
                       <textarea class="form-control" name="description" placeholder="Açıklama" required></textarea>
                   </div>
               </div>
               <div class="form-group row">
                   <label class="col-sm-4 col-form-label text-center">Tarih</label>
                   <div class="col-sm-6">
                       <input type="date" class="form-control" name="date" required>
                   </div>
               </div>
                    <div class="form-group row">

                            <label class="col-sm-4 col-form-label text-center">Yapılacak İşler</label>
                            <div class="clearfix"></div>
                            <div class="col-md-6 pull-left">
                                <input type="text" class="form-control" id="workName"
                                       placeholder="Yapılacak iş için başlık giriniz...">
                            </div>
                            <div class="col-md-2 pull-left">
                                <button type="button" class="btn btn-info" id="saveElement">Ekle</button>
                            </div>


                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label text-center">Yapılacak İşler</label>
                        <div class="col-md-6 pull-left">
                            <div id="elements" class="workCreateItems">


                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-block btn-info">Kaydet</button>


    </form>
            </div>
        </div>
    </div>


@endsection
@section('scripts')

    <script type="text/javascript">
    $(document).ready(function () {
    var controlList = [];
    $("#elements").sortable({
    placeholder: "sortable-placeholder",
    start: function (e, ui) {
    ui.placeholder.height(ui.item.outerHeight());
    },
    stop: function (event, ui) {
    }
    });


    $('#saveElement').click(function () {
    var workName = $('#workName').val();
    setControl(workName);
    });

    function generateString() {
    return Math.random().toString(36).substring(2);
    }

    function deleteElement(json, string) {

    $("[id='" + string + "']").parent('label').parent('div').fadeOut("normal", function () {
    $(this).remove();
    });

    return true;
    }
    function setControl(workName) {
    var generated = generateString();
    var newElement = '<div class="form-group elementItem">' +
        '<label class="custom-control custom-checkbox"> <input type="checkbox"  id="' + generated + '" name="work' + generated + '" class="form-check-input" disabled>' +
            '<span class="custom-control-indicator"></span><span class="custom-control-description">' + workName + '<input type="hidden" name="label' + generated + '" value="' + workName + '"/><span class="ml-2 delete fa fa-trash" element="' + generated + '"></span></span>' +
            '</label>' +
        '</div> ';
    $('#elements').append(newElement);
    controlList.push({
    'string': generated,
    'value': workName
    });
    console.log(controlList);
    $('#workName').val(null);
    }
    $(document).on('click', '.delete', function () {
    var r = confirm("Silmek istiyor musunuz?");
    if (r == true) {
    var element = $(this).attr('element');

    deleteElement(controlList, element);
    } else {
    return null;
    }
    });


    $('#testSubmit').click(function (e) {
    if ($("#test_form")[0].checkValidity()) {
    $(".loader").show();
    $('#testSubmit').hide();
    }
    });


    });
    </script>
    @endsection
