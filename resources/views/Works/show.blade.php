@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{$work->title}}</h6>
            </div>

                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-borderless p-0">
                                <thead>
                                </thead>
                                <tbody class="no-border-x">
                                <tr>
                                    <td width="10%"><b>Oluşturan</b></td>
                                    <td width="40%" class="user-avatar cell-detail user-info">
                                        <span class="cell-detail">{{\App\User::getUser($work->creator)->name}}</span>
                                    </td>
                                    <td width="10%"><b>Atanan</b></td>
                                    <td width="40%" class="user-avatar cell-detail user-info">

                                        <span class="cell-detail">{{\App\User::getUser($work->assigned)->name}}</span>


                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Durum</b></td>
                                    <td>{{getWorkStatus($work->status)}}</td>
                                    <td><b>Açıklama</b></td>
                                    <td colspan="3"><?php if (empty($work->description)) {
                                            echo '<p>Bir açıklama eklenmemiş :( !</p>';
                                        } else {
                                            echo $work->description;
                                        }
                                        ?></td>
                                </tr>

                                </tbody>
                            </table>

                            <table class="table table-striped table-borderless p-0">
                                <thead>
                              <th>
                                  Yapılacaklar
                              </th>
                                </thead>
                                <tbody class="no-border-x">
                                @foreach($workList as $element)
                                    <tr>
                                        <td class="description">
                                            <div class="form-group elementItem workCheckItem"
                                                 elementItem="{{$element->string}}"
                                                 id="{{$element->string}}">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox"
                                                           name="work{{$element->string}}"
                                                           id="{{$element->string}}" @if($element->value=="true") checked @endif
                                                           class="form-check-input element">
                                                    <span
                                                        class="custom-control-indicator"></span><span
                                                        class="custom-control-description">{{$element->description}}</span>

                                                </label>
                                            </div>
                                        </td>
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>
                        </div>


                        </div>
                    </div>
                </div>

        </div>


@endsection
@section('scripts')

    <script type="text/javascript">
        $(document).ready(function () {
            $(".element").click(function () {
                var check = $(this).prop('checked');
                var step = $(this).parents(".elementItem").attr('step');
                var string = $(this).parents(".elementItem").attr('elementItem');
                if (check == true) {
                    $.get("{{url('/works/'.$work->id.'/elements/')}}/" + string + '/true');
                    location.reload();

                } else {
                    $.get("{{url('/works/'.$work->id.'/elements/')}}/" + string + '/false');
                    location.reload();
                }

            });


        });
    </script>
@endsection
