@extends('layouts.master')

@section('content')

    <div class="container-fluid">

        <a href="{{url('works/create')}}" class="btn btn-info btn-icon-split mb-2">
                                        <span class="icon text-white-50">
                                            <i class="fas fa-plus"></i>
                                        </span>
            <span class="text">Yeni İş</span>
        </a>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">İşlerim</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-borderless" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Oluşturan</th>
                            <th>Başlık</th>
                            <th>Durum</th>
                            <th>Tarih</th>
                            <th></th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($works as $work)
                        <tr>
                            <td>{{\App\User::getUser($work->creator)->name}}</td>
                            <td>{{$work->title}}</td>
                            <td>{{getWorkStatus($work->status)}}</td>
                            <td>{{$work->date}}</td>
                            <td>
                                <a href="{{url('works/'.$work->string)}}" title="Detay"><i class="fa fa-eye"></i></a>
                                <a class="delete" title="Sil" id="{{$work->id}}"><i class="fa fa-trash"></i></a>
                            </td>

                        </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).on("click", ".delete", function() {
            var id=$('.delete').attr('id');
            $.ajax({
                type: "DELETE",
                url: '/works/' + id,
                data:{_token:'{{ csrf_token() }}'},
                success: function(result) {
                    location.reload();
                }
            });
        })

    </script>
    @endsection
