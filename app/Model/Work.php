<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Work extends Model
{
    protected $table='works';

    protected $fillable = ['string', 'title', 'description','creator','assigned','date'];


    public function  scopestring($query, $string)
    {
        return $query->where('string', '=', $string);
    }
    public function  scopeassigned($query, $assigned)
    {
        return $query->where('assigned', '=', $assigned);
    }


    public function workList()
    {
        return $this->hasMany(WorkList::class,'work','id');
    }

}
