<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WorkList extends Model
{
    protected $table='work_lists';

    public function  scopestring($query, $string)
    {
        return $query->where('string', '=', $string);
    }
}
