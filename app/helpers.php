<?php


function generateStringKey($length = 30)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
function getWorkStatus($status)
{
  if($status==1){
        echo'<span class="badge badge-pill badge-warning">Bekliyor</span>';
  }
  else if($status==2){
      echo '<span class="badge badge-pill badge-info">Yapılıyor</span>';
  }
  else if($status==3){
      echo '<span class="badge badge-pill badge-success">Tamamlandı</span>';
  }else{
      echo '<span class="badge badge-pill badge-dark">İptal Edildi</span>';
  }
}
