<?php

namespace App\Http\Controllers;

use App\Model\Work;
use App\Model\WorkList;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WorkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $user=Auth::user();
         $works=Work::assigned($user->id)->get();
         return view('Works.index')->with(['works'=>$works]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users=User::all();
        return view('Works.create')->with(['users'=>$users]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $title = $request->title;
        $description = $request->description;
        $assigned = $request->assigned;
        $date = $request->date;
        $validator = Validator::make([
            'title' => $request->title,
            'description' =>  $request->description,
            'assigned' =>  $request->assigned,
            'date' => $request->date,
        ],
            [
                'title' => 'required',
                'description' => 'required',
                'assigned' => 'required',
                'date' => 'required',
            ]
        );
        if ($validator->fails()) {
            $messages = $validator->messages();
            return redirect('works')->with(['type' => 'danger', 'title' => 'Hata!', 'message' => $messages]);
        } else {

            $work = new Work();
            $work->title = $title;
            $work->string = generateStringKey();
            $work->description = $description;
            $work->assigned = $assigned;
            $work->date = $date;
            $work->creator = Auth::user()->id;
            $work->save();

            $datas = $request->all();

            $dislanan = [
                '_token', 'date', 'label', 'description', 'title', 'assigned'
            ];


            foreach ($datas as $data => $key) {
                if (!in_array($data, $dislanan)) {

                    $input = trim($data, "label");
                    $elementDecs = $key;

                    $element = new WorkList();
                    $element->string = $input;
                    $element->description = $elementDecs;
                    $element->work = $work->id;
                    $element->save();

                }
            }
            return redirect('works');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $string
     * @return \Illuminate\Http\Response
     */
    public function show(string $string)
    {
        $work=Work::string($string)->first();
        $workList=$work->workList;
        return view('Works.show')->with(['work'=>$work,'workList'=>$workList]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        WorkList::where('work',$id)->delete();
        Work::find($id)->delete();
        return "success";
    }
    /**
     *
     *
     * @param  int  $work
     * @param  string  $string
     * @param  string  $value
     * @return \Illuminate\Http\Response
     */
    public function workElementTrue($work, $string, string $value)
    {
        $work = Work::find($work);


        $workList = WorkList::string($string)->first();


        if ($workList) {
            $workList->value = $value;
            $workList->save();


            $workListCount = WorkList::where('work',$work->id)->count();
            $testElementCheckedCount = WorkList::where('work',$work->id)->where('value','true')->count();

            if ($workListCount > 0) {
                if ($testElementCheckedCount == 0) {
                    $work->status = 1;
                } else {

                    if ($workListCount > $testElementCheckedCount) {
                        $work->status = 2;

                    } else if ($workListCount == $testElementCheckedCount) {
                        $work->status = 3;

                    } else {

                    }
                }
            }
            $work->save();

        }
        return 'success';
    }
}
